<!DOCTYPE html>
<html>
<head>
    <title>Calculator with History</title>
</head>
<body>
    <h1>Calculator</h1>
    <form action="CalculatorServlet" method="post">
        <input type="text" name="expression" placeholder="Enter expression" required>
        <input type="submit" value="Calculate">
    </form>

    <h2>Result:</h2>
    <div>
        <%= request.getAttribute("result") %>
    </div>

    <h2>Calculation History:</h2>
    <div>
        <%= request.getAttribute("history") %>
    </div>
</body>
</html>
