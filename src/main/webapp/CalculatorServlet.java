import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CalculatorServlet")
public class CalculatorServlet extends HttpServlet {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/calculator_db";
    private static final String DB_USER = "your_db_user";
    private static final String DB_PASSWORD = "your_db_password";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String expression = request.getParameter("expression");
        double result = calculate(expression); // Implement your calculation logic here
        
        // Insert the calculation into the database
        insertCalculation(expression, result);

        // Forward the result and history to the JSP
        request.setAttribute("result", result);
        request.setAttribute("history", getCalculationHistory());
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    private void insertCalculation(String expression, double result) {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            String insertQuery = "INSERT INTO calculations (expression, result) VALUES (?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
            preparedStatement.setString(1, expression);
            preparedStatement.setDouble(2, result);
            preparedStatement.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getCalculationHistory() {
        StringBuilder history = new StringBuilder();
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            String selectQuery = "SELECT expression, result FROM calculations ORDER BY id DESC LIMIT 10";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String expression = resultSet.getString("expression");
                double result = resultSet.getDouble("result");
                history.append(expression).append(" = ").append(result).append("<br>");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return history.toString();
    }
}
